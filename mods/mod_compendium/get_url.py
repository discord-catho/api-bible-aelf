import os
from dktorecuperedb.compendium import Compendium

def get_url(elt):

    coll = None
    ttl = None
    dis = None
    lang = None

    if isinstance(elt, dict):
        coll = elt['collection']
        ttl =  elt['title']
        dis = elt.get("disambiguation", None)
        lang = elt.get("language", None)
    elif isinstance(elt, Compendium):
        coll = elt.collection
        ttl = elt.title
        dis = elt.disambiguation
        lang = elt.language
    #endIf

    url = os.environ.get('API_URL')
    url += f"/v1/Compendium/{coll}/{ttl}"

    if dis is not None:

        url += f"?dis={dis}"

        if lang is not None:
            url +=f"&lang={lang}"
        #endIf

    elif lang is not None:
        url +=f"?lang={lang}"
    #endIf

    return url
#endDef
