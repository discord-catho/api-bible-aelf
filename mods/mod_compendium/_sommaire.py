import os
import hug
from dktorecuperedb.compendium import Sequence
from .get_url import get_url

def sommaire(self,
             name:str=None,
             ):

    """Return the sequence, without the content"""

    try:

        seq = Sequence(sequence_name=name)

        # { sequence_name:{id_key:{name:aa, same_page:t, gloria_patri:t}, ...}, index, status }

        content = {}

        content[name] = seq.get_sommaire()

        if content.get(name, None):
            content["index"] = {"0":{"url":f"{os.environ.get('API_URL')}/v1/Sommaire/{name}"}}
            content["status"] = "success"
        else:
            content["index"] = {}
            content["status"] = "error: datas not found, please check if name is in datas using 'index' "
        #endIf

    except:

        content = {"status":"failed", "error":"Unspecified error !"}
        raise Exception

    #endTry

    return content
#endDef
