import os
import hug
from dktorecuperedb.compendium import Sequence
from dktorecuperedb.compendium.db import CompendiumDB
def sequence_index(self):

    """Return the sequence, without the content"""

    try:
        s_db = CompendiumDB()
        unsorted_datas = s_db.get_sequence_index() # la, je l'ai deja triee par id<

        content = {
            elt[0]:{
                "title":elt[0],
                "url":f"{os.environ.get('API_URL')}/v1/Sequence/{elt[0]}"
            } for elt in unsorted_datas
        }

        if content:
            content["index"] = {"0":{"url":f"{os.environ.get('API_URL')}/v1/Sequence"}}
            content["status"] = "success"
        else:
            content["index"] = {}
            content["status"] = "error: datas not found, please check if name is in datas using 'index' "
        #endIf
    except Exception as e:

        content = {"status":"failed", "error":"Unspecified error !"}
    #endTry

    return content

#endDef
