import hug
from dktorecuperedb.compendium import Sequence

def sequence_sommaire(self,
                      sequence_name:str=None,
                      header_name: hug.types.text=None,
                      ):
    # dis = disambiguation
    # lang = language

    """Return a prayer"""
    if collection is None or name == "index" :
        name, dis = None, None
    elif name is None :
        name = None
    elif dis == "all" or lang == "all":
        dis, lang = None, None
    #endIf

    try:
        sequence = Sequence(sequence_name=sequence_name)

        content = {"index":[]}
        #TODO :
        # name :
        # | collectino_name : {key, name, dis, lang}
        for elt in comp.get_index():
            if "name" in elt.keys():
                content[elt["name"]] = {
                    (0 if not elt["name"] in content.keys() else len(content[elt["name"]])) :
                    {"name":elt["name"],
                     "disambiguation":elt["disambiguation"],
                     "language":elt["language"]
                     }
                }
            elif "key" in elt.keys():
                content[elt["key"]] = elt
                content["index"] += [elt["key"],]
            else:
                content["index"] += [elt,]
            #endIf
        #EndFor

        content["status"] = "success"

        #TODO : gerer si erreur (par exemple, pas de donnee trouvee)

        #content = f"Coucou compendium : collection={collection} ; name={name} ; disambiguation={dis} ; language={lang}"
    except:
        content = {"status":"failed", "error":"Unspecified error !"}
        raise Exception
    #endTry

    return content
#endDef
