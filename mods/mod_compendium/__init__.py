import hug
from lib.outputAPI import OutputAPI  # return : {status : success} a la place de {error : null}

from dktorecuperedb import Compendium
DEFAULT_LANG = "francais"
PREFIX = "Bibliotheque"

class ModuleCompendium:
        def __init__(self):
            return
        #endDef

        # COMPENDIUM is from dktorecuperedb

        from ._sommaire import sommaire
        from ._sequence import sequence
        from ._sequence_index import sequence_index
        # from ._sommaire_index import sommaire_index


        @hug.object.get(urls=f"/v1/{PREFIX}")
        def compendium_index_collection(
                        self,
                        key:hug.types.text=None,
                        usage:hug.types.text=None,
                        header_name: hug.types.text=None):

                if key is None and usage is None:
                        return Compendium().get_index()
                elif key is not None:
                        return Compendium(key=key).get_content()
                elif usage is not None:
                        return Compendium(usage=usage).collectionkey2data()
                return -1

        #endDef


        @hug.object.get(urls=f"/v1/{PREFIX}"+"/{collection}")
        def compendium_collection_index(self,
                                        collection:str=None,
                                        header_name: hug.types.text=None,
                                        exemples=f"/v1/{PREFIX}/Compendium"):
            return Compendium(collection=collection).get_index()
        #endDef


        @hug.object.get(
                urls=f"/v1/{PREFIX}"+"/{collection}/{name}",
        )
        def compendium_elements(self,
                                collection:str=None,
                                name:str=None,
                                dis:hug.types.text=None,#hug.types.one_of(['longue', 'courte'])=None,
                                lang:hug.types.text=None,# hug.types.one_of(['francais', 'latin'])=DEFAULT_LANG,
                                header_name: hug.types.text=None,
                                exemples=f"/v1/{PREFIX}/Compendium/pater_noster?dis=longue&lang=francais"
                                ):

            if name == "index":

                name = None
                return Compendium(collection=collection,title=name,disambiguation=dis,language=lang).get_index()

            else:

                 if dis == "all":
                            dis = None
                 #endIf
                 if lang == "all":
                            lang = None
                 #endIf

                 return Compendium(collection=collection,title=name,disambiguation=dis,language=lang).get_whatever_i_can()

            #else:
            #    return self.compendium_content(collection=collection,name=name,dis=dis,lang=lang,header_name=header_name)
            #endIf
            return {"status": "Unexpected error! mod_compendium"}
        #endDef

        # ####################################################"

        #@hug.object.get(urls="/v1/Sommaire")
        @hug.object.get(urls="/v1/Sequence")
        def sequence_index_seq(self,exemples="/v1/Sequence"):
            """Return sequence"""

            try:
                    content = self.sequence_index()
            except Exception as e:
                    content = {"status":"Unexpected error sequence_sommaire"}
                    print(e)
                    raise Exception
            #endTry

            return content
        #endDef

        @hug.object.get(urls="/v1/Sommaire/{name}",)
        def sequence_sommaire(self,
                              name: str=None,
                              exemples="/v1/Sommaire/priere_defunts"
                              ):
            """Return sequence"""

            try:
                content = self.sommaire(name)
            except:
                content = {"status":"Unexpected error sequence_sommaire"}
                raise Exception
            #endTry

            return content
        #endDef


        @hug.object.get(urls="/v1/Sequence/{name}",)
        def sequence_content(self,
                             name: str=None,
                             exemples="/v1/Sequence/priere_defunts"
                             ):

            try:
                if name == "index":
                    content = self.sequence_index(name)
                else:
                    content = self.sequence(name)
                #endIf

            except:
                content = {"status":"Unexpected error sequence_sommaire"}
                raise Exception
            #endTry

            return content
        #endDef


#endClass
