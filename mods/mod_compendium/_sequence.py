import os
import hug
from dktorecuperedb.compendium import Sequence
from .get_url import get_url

def sequence(self,
             name:str=None,
             ):

    """Return the sequence, without the content"""

    try:

        seq = Sequence(sequence_name=name)

        # { sequence_name:{id_key:{name:aa, same_page:t, gloria_patri:t, same_page, text, disambiguation, language, author, editor}, ...}, index, status }

        content = {}

        content[name] = seq.get_sequence()

        if content.get(name, None):
            content["index"] = {"0":{"url":f"{os.environ.get('API_URL')}/v1/Sequence/{name}"}}
            content["status"] = "success"
        else:
            content["index"] = {}
            content["status"] = "error: datas not found, please check if name is in datas using 'index' "
        #endIf

    except:

        content = {"status":"failed", "error":"Unspecified error !"}
        raise Exception

    #endTry

    return content
#endDef
