import hug
from dktorecuperedb.bible import Bible, BibleOneYear
from lib.outputAPI import OutputAPI  # TODO : return : {status : success} a la place de {error : null}

class ModuleBible:
    def __init__(self):
        return
    #endDef

    @hug.object.get(
        urls="/v1/Bible/{book}/{chapterverse}",
    )
    def bible_bchv(self, book:str=None, chapterverse: str=None, header_name: hug.types.text=None):
        """Return an extract of the Bible (AELF translation, in French), from a book and a reference

Usage : domains.com/v1/Bible/{book}/{chapterverse}

:param str book: Livre
:param str chapterverse: chapitre et verset
:param hug.type.text header_name: l'entete

:returns: TODO
:rtypes: dict
"""
        try:
            content = Bible(book, chapterverse)
            content.ref2text()
            out=OutputAPI( content=content.text, reference=chapterverse, errors=content.errors, credit="Bible: AELF (https://aelf.org)" )
        except:
            out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
            raise Exception
        #endTry

        return out.to_dict(removeEmpty=False)
    #endDef


    @hug.object.get(
        urls="/v1/daily-Bible/{day}",
    )
    def daily_bible(self,  day:int=None):
        """Return extracts from Bible, 3 or 4 differents each day to read the all Bible in one year"""

        if day is None:
            day = 1
        #endIf

        refsBible = BibleOneYear()
        return refsBible.on_a_year(day)
    #endDef
#endClass
