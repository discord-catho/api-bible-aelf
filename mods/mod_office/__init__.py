import sys

import os
import json
import hug

from datetime import datetime



if __name__=="__main__":
    sys.path.insert(0, '../..')
    from loadenv import LoadEnv
    LoadEnv(f"../../.env")
    from _calloffice import _calloffice, _callderouleoffice
else:
    from ._calloffice import _calloffice, _callderouleoffice
#endIF
from dktotoolkit.functions import compatMode
from dktotoolkit import discordify_dict

DEFAULT_OUTPUT_FORMAT="discord"
DEFAULT_OFFICE_ZONE="france"

class ModuleOffice:
    """
Retourner le contenu des offices
"""
    def __init__(self):
        return None
    #endDef


    @hug.object.get(
        urls="/v1/Office/{office}",
    )
    def office_short(
            self,
            date:str=None, calendrier: str=None,
            office: str=None,
            format_output=None,  # set with env variable if None
            deroule:bool=False,
            variante:bool=False,
            header_name: hug.types.text=None,
            **kwargs
    ):

        if format_output is None:
            format_output=os.environ.get("API_BIBLE_DEFAULT_OUTPUT",DEFAULT_OUTPUT_FORMAT)
        #endIf

        if calendrier is None:
            calendrier=os.environ.get("API_BIBLE_DEFAULT_CALENDRIER",DEFAULT_OFFICE_ZONE)
        #endIF

        if date is None:
            date = datetime.today().strftime('%Y-%m-%d')
        #endIf

        variante, kwargs = compatMode('variante', ['alternative', 'alternatives', 'variantes'], **kwargs)

        return _calloffice(
            date=date, calendrier=calendrier,  office=office,
            format_output=format_output,
            deroule=deroule,
            variante=variante,
            **kwargs)



    @hug.object.get(
        urls="/v1/OfficeAELF/{date}/{calendrier}/{office}",
    )
    def office(
            self,
            date:str=None, calendrier: str=None,  office: str=None,
            format_output:str=None,  # set with env variable if None
            deroule:bool=False,
            variante:bool=False,
            header_name: hug.types.text=None,
            **kwargs
    ):

        if not kwargs:
            kwargs = {}
        #

        # Compatibility mode here !
        if kwargs.get("format_discord") or kwargs.get("discord"):
            format_proper, kwargs_proper = compat_mode("format_output", ["format_discord","discord"], **kwargs)
            if format_proper:
                format_output, kwargs = format_proper, kwargs_proper
            #
        #

        if format_output is None:
            format_output=os.environ.get("API_BIBLE_DEFAULT_OUTPUT", DEFAULT_OUTPUT_FORMAT)
        #endIf

        variante, kwargs = compatMode('variante',  ['alternative', 'alternatives', 'variantes'], **kwargs)

        return _calloffice(
            date=date, calendrier=calendrier,  office=office, source="aelf",
            format_output=format_output,
            deroule=deroule,
            variante=variante,
            **kwargs
        )



    @hug.object.get(
        urls="/v1/OfficeAELF/deroule/{office}",
    )
    def derouleoffice(
            self,
            office: str=None,
            format_output:str=None,  # set with env variable if None
            deroule:bool=False,
            header_name: hug.types.text=None,
            **kwargs
    ):

        if format_output is None:
            format_output=os.environ.get("API_BIBLE_DEFAULT_OUTPUT",DEFAULT_OUTPUT_FORMAT)
        #endIf

        variante, kwargs = compatMode('variante',  ['alternative', 'alternatives', 'variantes'], **kwargs)

        return _callderouleoffice(
            office=office,
            format_output=format_output,
            deroule=deroule,
            **kwargs)
#endClass

if __name__=="__main__":
    date="2023-05-21"
    calendrier="france"
    office="laudes"

    m = ModuleOffice()
    #def __init__(self, format_output="html", date="today", calendar="francais", office=None, source="aelf"):
    #date:str=None, calendrier: str=None,  office: str=None, header_name: hug.types.text=None
    a = m.office(date=date, calendrier=calendrier,  office=office)
    print(a)
