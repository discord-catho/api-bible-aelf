import sys

from dktorecuperedb.office import Office
from lib.outputAPI import OutputAPI  # TODO : return : {status : success} a la place de {error : null}

# http://0.0.0.0:8000/v1/OfficeAELF/20-08-2022/france/vepres?format_output=markdown&discordwrap=true&discordwrap_width=1024

def _calloffice(
        date:str=None, calendrier: str=None,  office: str=None,
        format_output:str="html",
        source:str="aelf",
        deroule:bool=False,
        variante:bool=False,
        **kwargs):
    """Return an extract of the Bible (AELF translation, in French), from a book and a reference

Usage : domains.com/v1/Office/{date}/{calendrier}/{office}

:param str date: YYYY-MM-DD
:param str calendrier: france, romain, ...
:param str office: vepres, complies, ...
:param kwargs: Paramètres optionnels

:return: Résultat de l'API
:rtypes: dict
"""

    try:

        # format_output = "markdown"

        the_office = Office(format_output=format_output, date=date, calendar=calendrier, office=office, source=source)
    except:

        out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
        raise Exception

    #endTry

    try:
        the_office.check_input()
    except:

        out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
        raise Exception

    #endTry

    datas = {}

    if not deroule and not variante:
        try:
            datas = the_office.get_office(**kwargs)
        except:

            out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
            raise Exception

        #endTry

    elif deroule:

        try:
            datas = the_office.get_hunfolding(**kwargs)
        except:

            out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
            raise Exception

        #endTry

    elif variante:
        try:
            datas = the_office.get_alternatives(**kwargs)
        except:

            out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
            raise Exception

        #endTry
    #endIf

    try:
        out=OutputAPI(
            content=datas.get(office, {}),
            reference="AELF",
            credit="AELF (https://aelf.org)",
            informations=datas.get("informations", {}),
            status=datas.get("status", -1),
            errors=datas.get("error", None),
        )
    except:

        out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
        raise Exception

    #endTry

    return out.to_dict(removeEmpty=False)
#endDef



def _callderouleoffice(
        office: str=None,
        deroule:bool=True,
        format_output="html",
        **kwargs):
    """Return an extract of the Bible (AELF translation, in French), from a book and a reference

Usage : domains.com/v1/Office/deroule/{office}

:param str date: YYYY-MM-DD
:param str calendrier: france, romain, ...
:param str office: vepres, complies, ...
:param kwargs: Paramètres optionnels

:return: Résultat de l'API
:rtypes: dict
"""

    try:

        the_office = Office(format_output=format_output, date="today", calendar="romain", office=office, source="aelf")

    except:

        out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
        raise Exception

    #endTry

    try:

        the_office.check_input()

    except:

        out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
        raise Exception

    #endTry

    try:
        datas = the_office.get_hunfolding(**kwargs)
    except:

        out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
        raise Exception

    #endTry

    try:
        out=OutputAPI(
            content=datas[office],
            reference="AELF",
            credit="AELF (https://aelf.org)",
            informations=datas["informations"]
        )

    except:

        out=OutputAPI( content=None, reference=None, errors="Error", credit=None )
        raise Exception

    #endTry

    return out.to_dict(removeEmpty=False)
#endDef
