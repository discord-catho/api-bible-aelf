.. Gabriel documentation master file, created by
   sphinx-quickstart on Sun May 21 00:31:20 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gabriel's documentation!
===================================

.. IMPORTANT::
   Gabriel :
   **G**\ éniale **A**\ PI (b?)(pour)  p**i**\ er, s'**e**\ difier en **l**\ isant / **l**\ ouant


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   src/lib/office
   src/lib/compendium
   src/lib/bible
   src/mods/mod_office

   officebibleASPLITTER

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
