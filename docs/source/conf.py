# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(0, os.path.abspath('../..'))

os.environ["PYTHONPATH"] = ".."

for x in os.walk('../../lib'):
  sys.path.insert(0, x[0])


# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Gabriel'
copyright = '2023, Pierre'
author = 'Pierre'
release = 'v1.1-beta'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
  'sphinx_rtd_theme',
  'sphinx.ext.autodoc',  # Autodoc
  'sphinx.ext.autosummary',  # Ne pas ecrire les autoX
  #'sphinx_automodapi.automodapi',
  #'sphinxcontrib.httpdomain', #API
  "myst_parser",    # Add the README
]

# highlight_language = ['python', 'http'] # reconnaître et mettre en évidence le code HTTP avec la directive http:highlight.

#spécifier le nom du répertoire où les pages de documentation seront générées
automodapi_toctreedirnm = 'api'

templates_path = ['_templates']
exclude_patterns = []

language = 'fr'

os.environ['SPHINX_BUILD'] = '1'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

autodoc_default_options = {
    'members': True,
    'member-order': 'bysource',
    'special-members': '__init__',
    'undoc-members': True,
    'exclude-members': '__weakref__'
}
