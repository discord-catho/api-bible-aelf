import os, sys
from list_books import BIBLE_BOOKS, BIBLE_PSALMS, BIBLE_BOOKS_ACR_EN_FR


class ReferenceBible:
    def __init__(self, book=None, chapter=None, start_verse=None, end_chapter=None, end_verse=None):
        self.book = book
        self.chapter = chapter
        self.end_chapter = end_chapter
        self.start_verse = start_verse
        self.end_verse = end_verse
    #endDef

    def to_list(self):
        return [self.book, self.chapter, self.start_verse, self.end_chapter, self.end_verse]
    #endDef

    def parse_book_name(self):


        if self.book[:2] == "Ps":
            self.book = f'Ps{"".join(self.book.split(" ")[1:])}'

        else:
            for val in BIBLE_BOOKS_ACR_EN_FR.values():
                for val_cat in val.values():
                    for key_book, val_book in val_cat.items():
                        if self.book in [e.strip() for e in val_book]:
                            self.book = key_book
                    #endFor
                #endFor
            #endFor
        #endIf


        list_accronyms = []

        for val in BIBLE_BOOKS.values():
            for val_cat in val.values():
                list_accronyms += list(val_cat.keys())
            #endFor
        #endFor

        list_accronyms += list(BIBLE_PSALMS.keys())

        exceptions = []
        if not self.book in list_accronyms+exceptions:
            print(f"!> Accronyms: {list_accronyms}")
            print()
            print()
            raise ValueError(f"{self.book} not in the list of accronyms")
        #endIf
#endClass



class ReferencesOfTheDay:
    def __init__(self, day=None, ref1=None, ref2=None, ref3=None, ref4=None):
        r1 = ref1
        r2 = ref2
        r3 = ref3
        r4 = ref4

        self.references = [r1, r2, r3, r4]
        try:
            self.day = int(day)
        except:
            self.day = day
        #endTry
    #endDef

    def append(self, ref=None):
        added = False
        empty = ReferenceBible()
        for i in range(len(self.references)):
            if added :
                continue
            #endIf

            if isinstance(ref, (list, tuple)):
                for each_ref in ref:
                    self.append(each_ref)
                #endFor
                added = True
            elif not self.references[i]:
                self.references[i] = ref
                added = True
            #endIf
            #endIf
        #endFor

        if not added :
            self.references += [ref,]
        #endIf
    #endDef

    def to_list(self):
        out = [self.day,]

        empty = ReferenceBible()

        j = 0

        for elt in self.references:
            if elt is None :
                continue
            #endIf
            out += elt.to_list()
            j += 1

        #endFor
        while j<5:
            out += empty.to_list()
            j+=1
        #endW

        return out
    #endDef

    def to_lists(self):
        out = []

        empty = ReferenceBible()

        j = 0

        for elt in self.references:
            if elt is None :
                continue
            #endIf
            print(elt.__dict__)
            out += [(self.day, j,  *elt.to_list()),]
            j += 1

        #endFor

        return out
    #endDef
#endClass
