This script is used to create a database from a csv file for a daily Bible reading (over 1 year).

## Input datas
Schedule-of-Readings-Bible-One-Year-Chronological.pdf

* Extraction using Okular
* Renaming book names using OnlyOffice