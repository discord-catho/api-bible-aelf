from convert_csv import csv2query
from creat_db import tables_to_sqlite


def main():

    #csv_file="./input_datas/Schedule-of-Readings-Bible-One-Year-Chronological_fr.csv"
    #db_file="../../datas/bible_1_an_fr.db"

    csv_file="./input_datas/Schedule-of-Readings-Bible-One-Year-Chronological.csv"
    db_file="../../datas/bible_1_an.db"

    queries = csv2query(csv_file)

    tables_to_sqlite(queries, db_file=db_file)

    print("> Done !")
#endDef

if __name__=="__main__":
    main()
