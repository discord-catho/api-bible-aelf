import csv


from parse_ref import parse_ref
from reference import ReferenceBible, ReferencesOfTheDay


def csv2query(csv_file):

    queries = []

    # Open the CSV file
    with open(csv_file, newline='') as f:
        reader = csv.reader(f)

        # Skip the header row
        #next(reader)

        # Iterate over the rows in the CSV file

        for row in reader:
            if len(row) == 1:
                rows = row[0].split(";")
                #endIf
            elif len(row) > 1:
                rows = ",".join(row).split(";")
            else:
                rows = row
            #endIf

            day = rows[0]
            query_refs = ""

            refday = ReferencesOfTheDay(day=day)
            for data in rows[1:]:

                ref = parse_ref(data)

                if ref is None:
                    continue
                #endIf

                refday.append(ref)

            #endFor data
            print(refday.to_lists())
            queries += [refday.to_lists(),]
        #endFor row
    #endWith

    return queries
#endDef
