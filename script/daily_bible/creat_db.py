import sqlite3

def tables_to_sqlite(queries, db_file="./bible1year.db"):

    # Connect to the SQLite database
    conn = sqlite3.connect(db_file)
    c = conn.cursor()

    # Delete if the table exists in the database
    query_delete_table = '''DROP TABLE IF EXISTS bible_references'''
    c.execute(query_delete_table)

    # Create the table in the database
    query_create_table = '''CREATE TABLE bible_references
(day INTEGER, id INTEGER,
book TEXT, chapter INTEGER, start_verse INTEGER, end_chapter INTEGER, end_verse INTEGER
)'''
    c.execute(query_create_table)



    query = "INSERT INTO bible_references(day, id, book, chapter, start_verse, end_chapter, end_verse) VALUES (?,?, ?,?,?,?,?)"

    i = 0
    for data in queries:
        for elt in data:
            datasA = [elt[0], i, *elt[2:]]
            print(query, datasA)
            c.execute(query, datasA)
            i+=1
        #endIf
    #endFor
    conn.commit()
    conn.close()
#endDef
