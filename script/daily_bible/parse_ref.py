import re

from reference import ReferenceBible


def parse_ref_book(ref_str):

    ref_str_1 = ref_str.strip().split(" ")

    book = ""
    ref = []

    if ref_str_1[0].isdigit():
        if ref_str_1[1].lower() in ["épître",]:
            i = 4
        else:
            i = 2
        #endIf
    elif ref_str_1[0].lower() in ["psalm", "psaume", "Ps", "ps"]:
        i = 1
    elif ref_str_1[0].lower() in ["épître",]:
        i = 3
    else:
        i = 1
    #endIf

    if not book:
        book = " ".join(ref_str_1[0:i])
    #endIf

    if not ref and len(ref_str_1) > i:
        ref = " ".join(ref_str_1[i:]).split('-')
    #endIf

    return book.strip(), [e.strip() for e in ref]
#endDef


def parse_ref(ref_str):
    ref_str = ref_str.strip()
    if not ref_str:
        return None
    elif "," in ref_str:
        if not ref_str.split(",")[1].strip().isdigit():
            return [parse_ref(e) for e in ref_str.split(",")]
        elif not "Sir" in ref_str:
            raise ValueError(f"Comma inside: {ref_str}")
        #endIf
        #raise ValueError("Seulement les intervales sont possibles ici !")
    elif ref_str[:2] == "Ps" and "-" in ref_str and not ":" in ref_str:
        return [parse_ref(e) if "Ps" in e else parse_ref("Ps "+e) for e in ref_str.split("-")]
    elif ref_str in ["Ps 9", "Ps 113"]:
        return [parse_ref(ref_str+"A"), parse_ref(ref_str+"B")]
    #endIf

    book, ref_parts = parse_ref_book( ref_str)

    ref = ReferenceBible(book=book)
    ref.parse_book_name()

    if len(ref_parts) == 1:

        if ":" in ref_parts[0]:

            ref.chapter, ref.start_verse = ref_parts[0].split(':')
            ref.chapter, ref.start_verse = int(ref.chapter), int(ref.start_verse) if ref.start_verse else None

        elif ref_parts[0].isdigit():

            ref.chapter = int(ref_parts[0])

        else:

            ref.chapter = ref_parts[0]

        #endIf

    elif len(ref_parts) == 2:

        first_part, second_part = ref_parts

        if ":" in first_part:

            ref.chapter, ref.start_verse = first_part.split(':')

        else:

            ref.chapter = first_part
            ref.start_verse = None

        if ':' in second_part:

            ref.end_chapter, ref.end_verse = second_part.split(':')

        elif ":" in first_part:

            ref.end_chapter = ref.chapter
            ref.end_verse = second_part

        else:

            ref.end_chapter = second_part
        #endIf
    #endIf

    return ref
#endDef


if __name__=="__main__":
    Ls = [#"1 Jean 3:5,6",
        "1Jean 1:3-6",
        "1 Pierre 1:3-4:6",
        "Matthieu 3:5"
    ]
    for e in Ls:
        print(parse_ref(e))
    #endFor
