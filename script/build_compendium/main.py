import json
import os
import sqlite3

from models import Content, Sequence, SequenceItem


def auto_generate(
        path_db="database.db",
        path_files="json_files",
        add_content = True,
        add_sequence = True
):

    conn = sqlite3.connect(path_db)

    # Create tables
    content = Content(conn)
    content.creat_table()
    del content

    sequence = SequenceItem(conn)
    sequence.creat_table()
    del sequence


    thedir = path_files
    for file_name in os.listdir(thedir):
        print()
        #print(">", file_name)

        if os.path.isdir(f'{thedir}/{file_name}'):
            continue
        elif not ".json" in file_name:
            continue
        #endIf

        with open(f'{thedir}/{file_name}') as file:
            try:
                data = json.load(file, strict=False)
            except :
                print("!!>", file_name)
                continue
            #endTry
            #print(">", data)

            l_name_sequence=[e for e in data.keys() if e not in  ["deroulement","informations", "information"]]
            name_sequence = l_name_sequence[0] if len(l_name_sequence) > 1  else file_name.replace(".json", "")

            if add_content:
                content = Content(
                    conn=conn,
                    title=name_sequence,
                    data=data.get(name_sequence, {}),
                    update=False
                )
                content.check_and_insert()
                del content
            #endIf
            if add_sequence and "deroulement" in data.keys():
                informations=data.get("information", [])
                informations=data.get("informations", []) if not informations else informations

                sequence = Sequence(
                    conn=conn,
                    title=name_sequence,
                    sequence=data["deroulement"],
                    informations=informations,
                    update=False
                )
                # sequence.check_and_insert_sequence()

                del sequence
            #endIf
        #endW
        print()
        print("______________________________________")
        print()
        print()
        print()
    #endF
    conn.commit()
    conn.close()

#endDef

def manual_edition(**kwargs):
    True
#endDef



if __name__=="__main__":
    for elt in ["compendium", "hymne", "hymne_mariale", "cantique", "additionnals", "office"]:
        auto_generate(
            path_db='../../datas/compendium.db',
            path_files=f"../../../bot_breviaire_v2/datas/{elt}"
        )
    #endFor

    manual_edition(
        path_db='../../datas/compendium.db',
        path_files='../../../bot_breviaire_v2/datas/compendium'
    )


    # SELECT * from sequence LEFT JOIN content ON content.key = sequence.key_name WHERE sequence.sequence_name = "laudes" ORDER BY  LENGTH(sequence.id_key) ASC, sequence.id_key ASC
    # ORDER BY CAST(category_id AS UNSIGNED INTEGER)

    #TODO : pour les hymnes :
    # - title = titre de l'hymne
    # - reference = Hymne

    #https://sqlpro.developpez.com/cours/sqlaz/jointures/#LI
    #https://www.tutorialspoint.com/sql/sql-left-joins.htm#:~:text=The SQL LEFT JOIN returns,column from the right table.
