def parse_key_content(self, title):

    if self.title and self.collection :
        self.key = f"{self.title}_{self.collection}".replace("(", "_").replace("(", "").lower()
    else:
        self.key = f"{title.lower()}"
    #endIf

#endDef
