def update_entry(self):
    content_data = {k:v for k,v in vars(self).items() if k[0] != "_"}
    # génération de la requête d'insertion
    query = f"UPDATE {self._db_name} SET "
    query += ", ".join([f"{e} = ?" for e in content_data.keys()])
    query += "WHERE key = ?"

    # execution de la requête
    print(f"UPDATE_CONTENT> {query}")
    print(f"..............> {tuple(content_data.values())}")
    cursor = self._conn.cursor()
    cursor.execute(query, (*content_data.values(), self.key))
    self._conn.commit()

#endDef
