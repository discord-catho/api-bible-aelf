class DB_Builder:
    def __init__(self,
                 conn=None,
                 db_name="database",
                 virtual_db = False,
                 update=True
                 ):
        self._conn = conn
        self._db_name = db_name
        self._virtual_db = virtual_db

        self._update = update
        self._skip = False
    #endDef

    # DB methods
    from .is_entry_in_db import is_entry_in_db
    from .insert_entry import insert_entry
    from .update_entry import update_entry
    from .find_missing_values import find_missing_values
    from .handle_similar_entry import handle_similar_entry
    from .check_and_insert import check_and_insert

    from .creat_table import creat_table
#endDef
