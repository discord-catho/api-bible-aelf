def find_missing_values_simple(self):

    missing_values = {}

    for variable_name, variable_value in vars(self).items():
        if variable_value is None:
            missing_values[variable_name] = input(f"Enter a value for {variable_name}: ") or None
        #endIf
    #endFor

    return missing_values
#endDef

def find_missing_values(self):
    missing_values = {}

    for variable_name, variable_value in vars(self).items():
        if variable_value is None:
            cursor = self._conn.cursor()
            query = f"SELECT {variable_name} FROM {self._db_name} GROUP BY {variable_name}"
            print(f"FIND_MISS_DATA>  {query}")
            cursor.execute(query)
            db_value = [e[0] for e in cursor.fetchall()]

            if db_value:

                and_again = True
                while and_again:
                    print(f"MISSING_VALUE> _{self.key}_:_{variable_name}_")
                    print(f"             > Present values: {[v for k, v in vars(self).items() if k[0] != '_']}")
                    print("Values for {} was found in the database:".format(variable_name))
                    print("To reuse a value, please enter his id: {}".format(dict(enumerate(db_value))))
                    print("If you don't want reuse a value, please use [n] to enter a new value or [s] to skip or [e] for an empty value")
                    use_db_value = input(f"({self.key}) Do you want to use a value? (id/n)")

                    if not use_db_value.isdigit():
                        and_again = use_db_value not in ["n", "q", "s", 'e']
                    else:
                        and_again = int(use_db_value) not in dict(enumerate(db_value)).keys()
                    #endIf

                #endWhile

                if use_db_value.isdigit():
                    setattr(self, variable_name, db_value[int(use_db_value)])
                    self._skip = False
                elif use_db_value.lower() in ['n',]:
                    user_input = input("Enter a new value for {}: ".format(variable_name))
                    setattr(self, variable_name, user_input)
                    self._skip = False
                elif use_db_value.lower() in ['e',]:
                    setattr(self, variable_name, None)
                    self._skip = False
                elif use_db_value.lower() in ['q',]:
                    import sys
                    print("You asked to finish. Bye ;)")
                    sys.exit()
                elif use_db_value.lower() in ['s',]:
                    self._skip = True
                    return
                else:
                    print(f"Wrong choice `{use_db_value}`! I'll not modify the value!")
                #endIf

            else:
                user_input = input("Enter a value for {}: ".format(variable_name))
                setattr(self, variable_name, user_input)
                self._skip = False
            #endIf
        #endIf
    #endFor
    #self._conn.commit()

    return missing_values
