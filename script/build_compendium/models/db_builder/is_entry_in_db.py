def is_entry_in_db(self):

    c = self._conn.cursor()
    c.execute(f"SELECT * FROM {self._db_name} WHERE key = ?", (self.key,))
    result = c.fetchone()

    return True if result else False
#endDef
