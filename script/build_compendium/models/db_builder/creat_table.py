from .type2typesql import type2sqltype

def creat_table(self):

    content_data = {k:type(v) for k,v in vars(self).items() if k[0] != "_"}

    query = f"CREATE TABLE IF NOT EXISTS {self._db_name} ("
    query += "key TEXT PRIMARY KEY, "
    #query += ", ".join([f"{e} TEXT NOT NULL" for e in content_data.keys() if e != "key" and e[0] != "_"])
    query += ", ".join([f"{e} {type2sqltype(v)}" for e,v in content_data.items() if e != "key"])
    query += ")"
    print(f"CREAT_TABLE> {query}")
    c = self._conn.cursor()
    c.execute(query)

    # GetColumns of the database :
    query = f"PRAGMA table_info({self._db_name})"
    c.execute(query)
    list_columns = [e[1] for e in c.fetchall()]

    missing_cols = {k: content_data[k] for k in list(set(list_columns) ^ set(content_data))}
    print(missing_cols)

    # Add columns if one is missing
    if missing_cols:
        str_add_cols = ", ".join([f"{e} {type2sqltype(v)}" for e, v in missing_cols.items()])
        query = f"ALTER TABLE {self._db_name} ADD COLUMN {str_add_cols};"
        c.execute(query) #, (None,)*len(missing_cols))
    #endIf

    self._conn.commit()

#endDef
