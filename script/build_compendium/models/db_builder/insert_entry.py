def insert_entry(self):
    content_data = {k:v for k,v in vars(self).items() if k[0] != "_"}
    # génération de la requête d'insertion

    query = f"INSERT INTO {self._db_name} ( "
    query += ', '.join(content_data)
    query += ") VALUES ("
    query += ', '.join('?'*len(content_data))
    query += ")"
    # execution de la requête

    print(f"INSERT_CONTENT> {query}")
    print(f"..............> {tuple(content_data.values())}")
    cursor = self._conn.cursor()
    try:
        cursor.execute(query, tuple(content_data.values()))
    except Exception as e:
        print(query)
        print(e)
        raise(Exception)
    self._conn.commit()
#endDef
