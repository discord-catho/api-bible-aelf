def handle_similar_entry(self)->bool:
    '''
@output: boolean : True if the entry must be update
'''

    content_data = {k:v for k,v in vars(self).items() if k[0] != "_"}
    list_var_content_data = list(content_data.keys())

    cursor = self._conn.cursor()
    query  = f"SELECT "
    query += ", ".join(list_var_content_data)
    query += f" FROM {self._db_name} WHERE key = ?"

    print(f"HANDLE_SIMILAR> {query}")
    cursor.execute(query, (self.key,))
    similar_entry = cursor.fetchone()

    #content_data = {k:v for k,v in vars(self).items() if k[0] != "_"}

    if similar_entry:
        # Compare the text and reference of the entry
        #TODO : passer sur de l'independant de l'objet !

        print("Similar entry found with key: ", self.key)

        diff = {}
        for i in range(len(list_var_content_data)):
            entry_name = list_var_content_data[i]
            self_value = content_data[entry_name]
            if isinstance(self_value, bool):
                self_value_str = "1" if self_value else "0"
            elif isinstance(self_value, (int, float)):
                self_value_str = str(self_value)
            else:
                self_value_str = self_value
            #endIf

            diff[entry_name] = []
            if self_value_str is not None and similar_entry[i] is not None:
                diff[entry_name] = set(self_value_str.split()) ^ set(similar_entry[i].split())
            #endIf
        #endFor

        if [e for e in diff.values() if e]:
            for k, v in diff.items():
                print(f"Difference in {k} : ", ', '.join(v))
            #endFor
        else:
            print("NO difference between the file and the entry, but you always manually edit the entry if there is missing values")
        #endIf

        # Ask user if they want to update the entry or not
        answer = ""
        while answer.lower() not in ["y", "o", "n"]:
            #print(f"SIMILAR_ENTRY> {self.key}")
            answer = input(f"({self.key}) Do you want to update the entry ? (y/n)  [TODO: dissocier les valeurs] ")
            if answer.lower() == "q":
                import sys
                print("Bye (handle similar!)")
                sys.exit()

            elif answer.lower() in ["y", "o"]:
                return True
            #endIf
        #endW
        #answer = 'y'


    else:
        return True
    #endIf
#endDef
