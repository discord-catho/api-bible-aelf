def type2sqltype(t)->str:
    if t is type(None):
        return ""
    elif t is str:
        return "TEXT"
    elif t is bool:
        return "BOOLEAN"
    elif t is int:
        return "INTEGER"
    elif t is float:
        return "REAL"
    elif t is dict:
        raise TypeError
    elif t is list or t is tuple:
        print("TYPE 1", t)
        raise TypeError
    else:
        print("TYPE 2", t)
        raise ValueError
    #endIf
    print("TYPE 3", t)
    raise Exception
#endDef
