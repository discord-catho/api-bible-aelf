import re

def parse_data_content(self, data):
    #TODO : chercher si un meme titre existe et si c'est le cas, demander si par defaut.
    if isinstance(data, dict):

        title_tmp = ""
        for elt in ["titre", "title"]:
            title_tmp = title_tmp if title_tmp else data.get(elt, None)
        #endFor

        if title_tmp :

            if "(" in title_tmp or "_" in title_tmp or ":" in title_tmp:
                splitted_title = re.split('\(|\)|_|\:', title_tmp)
                self.title = splitted_title[0]
                self.disambiguation = " ".join([e.strip() for e in splitted_title[1:] if e])
            else:
                self.title = title_tmp
            #endIf

        else:

            self.title = title_tmp

        #endIf

        for elt in ["text", "texte"]:
            self.text = self.text if self.text else data.get(elt, None)
        #endFor


        for elt in ["reference", "ref"]:
            self.collection = self.collection if self.collection else data.get(elt, None)
        #endFor

    elif isinstance(data, str):

        self.text = data
        self.title = None
        self.collection = None

    #endIf
#endDef
