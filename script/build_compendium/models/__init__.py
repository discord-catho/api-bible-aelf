import re
from .db_builder import DB_Builder

class Content(DB_Builder):
    #def __init__(self, conn=None, name:str="unknown_key", data: Union[dict, str]={}):
    def __init__(self,
                 conn=None,
                 title:str="unknown_name_sequence",
                 data: dict={},
                 db_name="content",
                 update=True
                 ):

        super().__init__(conn=conn, db_name=db_name, update=update)

        # Datas:
        self.key = "unknown_key"
        self.title = ""
        self.text = ""
        self.collection = ""  # ex reference  (compendium, priere, hymne, psaume, cantique)
        self.disambiguation = ""
        self.language = ""
        self.author = ""
        self.editor = ""

        self.parse_data_content(data)
        self.parse_key_content(title)

        print("KEY>", self.key)
        print(f"INPUT> {vars(self)}")

    #endDef

    # Methods:
    from .content_parse_data import parse_data_content
    from .content_parse_key import parse_key_content
#


class SequenceItem(DB_Builder):
    def __init__(self,

                 conn=None,
                 db_name="sequence",
                 virtual_db=False,
                 update=True,

                 sequence_name:str="unknown_sequence_name",

                 key:str="unknown_key",
                 name:str="unknown_name",
                 same_page:bool=False,
                 id_key:int=0,
                 gloria_patri:bool=False,
                 default:bool=False
                 **kwargs
                 ):
        super().__init__(conn=conn, db_name=db_name, virtual_db=virtual_db, update=update)

        self.sequence_name = sequence_name
        self.key = f"{sequence_name}_{id_key}_{key}"
        self.key_name = str(key)
        self.name = str(name)
        self.same_page = bool(same_page)
        self.gloria_patri = bool(gloria_patri)
        self.id_key = int(id_key)
        self.default = bool(default)

        if kwargs :
            print(f"{__file__} : KWARGS ! {kwargs}")
        #endIf
    #endDef

#endClass

class Sequence:
    def __init__(self,
                 conn=None,
                 db_name="sequence",
                 sequence:list=[],
                 informations:dict={},
                 title:str="unknown_name_sequence",
                 virtual_db=False,
                 update=True
                 ):

        id_replacement = 0
        for elt in sequence:
            if not "key" in elt.keys() or elt["key"] is None:
                print("SKIPPPPPPPPP", elt)
                continue
            #endIf

            if not "id_key" in elt.keys():
                elt["id_key"] = id_replacement

            else:
                id_replacement = elt["id_key"]
            #endIf

            id_replacement += 1

            s = SequenceItem(conn=conn, db_name=db_name, sequence_name=title, update=update, **elt)
            s.check_and_insert()
            del s
        #

        #
    #endDef

    # Methods:
    # from .sequence_fetch_datas import fetch_datas_sequence
    from .sequence_add import add_sequence

#

class Mystery(DB_Builder):
    def __init__(self,
                 conn=None,
                 db_name="sequence"
                 ):
        super().__init__(conn=conn, db_name=db_name)

        # Datas inside sequence
        self.key = ""
        self.name = ""
        self.id_mystery = 0
        self.bible_ref = ""
        self.id_key = 0
        self.fruit = ""
        self.comment = ""

    #endDef
#endClass
