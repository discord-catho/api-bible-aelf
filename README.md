# API Bible AELF

API to get the AELF Bible from other projects.

# How to use it?
- Requirement : Hug, Six

- Using venv (`$ pip install venv`)
```
$ cd /in/the/repo/dir/api-bible-aelf
$ python -m venv env_api
$ source env_api/bin/activate
$ pip3 install -r requirements.txt
```

- Clone the repositery
```
$ git clone git@framagit.org:discord-catho/api-bible-aelf.git

$ git submodule update --recursive --init
$ git submodule update --recursive --remote
```

- Launch
```
$ cd /in/the/repo/dir/api-bible-aelf
$ source env_api/bin/activate
```

and then
```
$ hug -f main.py
```
or (last version of Hug)
```
$ hug -f main
```

('-f' because the file is specified)

# To Do

## NEXT STEP : office
- [ ] Office DB must be clean !!!
  - [X] Remove duplicates from office hunfolding (to be improve with SQL but, it s working now)
  - [x] Delete old offices (previous days generated AND dated)
  - [ ] Add uniq ids
  - [ ] Allow requests on the 5 previous / 5 next months
  - [ ] Save psaumes (for recitation) and hymnes to additionnal DB
  - [ ] Eviter doublons
    - [ ] Creer une DB [office_cleelements] = (id_office, id_element, cle_element)
    - [ ] Supprimer de [office_element] : (id_office, nom_office, cle_element)
    - [ ] Ajouter dans [office_element] : (id_element)  <= index
    - [ ] Requetes
      - [ ] Ajouter a chaque fois un element dans [office_cleelements]
      - [ ] Ne pas ajouter dans [office_element] un element deja present
      - [ ] Pour le deroulement et rechercher les titres, merge les 2 sur (cle_element)
  - [X] Ajouter requete http://localhost/zone/date/office?hymne=keyhymne&hymne_mariale=key_hymnemariel
- [ ] DB Bible
  - [ ] Allow several Bible
  - [ ] Reference : add the book name in return
- [ ] Merge BD
  - [ ] Autobackup DB compendium and Bible
  - [ ] Merge DBs
- [ ] Split DB and API in 2 repos
- [ ] DB clean, with primary key, foregin key reference, ...
- [ ] Ne pas avoir a ajouter les elements supplementaires a la table "office_addition" => pas possible d outer join, donc il va falloir fusionner les tables offices et compendium
- [ ] Ajouter key_translate a content / content_collection + gestion des traductions

## Files
- [ ] LICENCE
- [ ] README
- [ ] dotenv

## Code
- [X] return reference : use chapter before verse
- [ ] http://localhost:8000/v1/Bible/Jn/1:48-50,5  :  the last 5 must be the chapter
- [X] Add date of generation of the DB
- [ ] Create submodule for
  - [-] html parser
  - [ ] loadenv file
- [ ] Unitary tests

## Host
- [ ] Host on VPS
- [ ] Creat domain


## Datas
- [ ] Add compendium
- [ ] Add chapelet
- [ ] Add hymns
- [-] Add Bible on 1 year
***

# Licence
GNU AFFERO GENERAL PUBLIC LICENSE
