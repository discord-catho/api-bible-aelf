
if __name__=="__main__":
    import os, sys

    path=os.path.dirname(os.path.abspath(__file__))
    sys.path.insert(0, os.path.join(path, "../../bot_breviaire_v2/lib/usefull"))
    import loadenv
    print(os.environ)
    loadenv.load(f"{path}/../.env")

    path=os.path.dirname(os.path.abspath(__file__))
    sys.path.insert(0, os.path.join(path, ".."))
    from lib.bible.db import BibleDB

    bible=BibleDB()

    path=None

    # Debut:
    from lib.bible import Bible

    #for refs in ["1", "1:2", "1:3-6", "2:1,4", "2:3,3:4", "5:2-6:7", "10:11-13-15", "20:21,24-26,39"]:
    for refs in ["1:2", "1:3-6", "2:1,4", "2:3,3:4", "5:2-6:7", "10:11-13-15", "20:21,24-26,39"]:
        b = Bible("Jn", refs)
        print(b.__dict__)
        print(b.ref2text())
        print()
    #endFor

    bible.end_conn()
    print(">>> End")
#endIf
