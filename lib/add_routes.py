from hug import interface as hug_interface

def add_routes(api):
    """Automatically add routes from API
    This function automatically adds routes from an API to the HTTP server.

    Args:
        api (hug.API): The Hug API instance.
"""
    print("=============")
    print(api.module)
    print()
    for name, obj in vars(api.module).items():
        if isinstance(obj, (str, dict)) or obj is None:
            continue
        #endIf
        print(name, ">>>", obj, " ... ", type(name), type(obj))
        #print(obj.__module__)
        if isinstance(obj, hug_interface.Interface):
            print("..", obj.__module__)
            #api.http.add_route("/",obj)
            api.http.add_routes(obj)
        #endIf

    #endFor
#endDef


"""
>> Ca ne fonctionne pas !
Fichier module (https://beta.openai.com/playground)
import hug

def add_routes(api):
    "Automatically add routes from API"
    for name, obj in vars(api.module).items():
        if isinstance(obj, hug.interface.Routes):
            api.http.add_routes(obj)

@hug.get("/v1/{year}/{username_optional?}")
def say_hello(year: int, username_optional: str = None):
    "Return a friendly message when visiting /2023/openai"
    if username_optional:
        return f"Hello {username_optional} et bonne année {year}!"
    else:
        return f"Bonne année {year}!"

@hug.get("/v2/{name}")
def say_hi(name: str):
    "Return a friendly message when visiting /v2/{name}"
    return f"Hi {name}!"

Fichier main.py :

import hug

from exemple import say_hello, say_hi

api = hug.API(__name__)
add_routes(api)

if __name__ == '__main__':
    api.http.serve()
"""
