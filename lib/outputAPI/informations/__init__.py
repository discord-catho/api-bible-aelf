import os
import time

class OutputAPI_informations:
    """
Informations de sortie supplémentaire :

* date_creation_database
* url_api
* licence
"""
    def __init__(self, licence:str=None):
        """
Initialisation

:param str licence: Licence
"""
        self.date=time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(os.stat(os.environ.get("RECUPEREDB_BIBLE_PATH")).st_ctime))

        self.url=os.environ.get("API_BIBLE_URL")

        if licence is not None:
            self.licence = f"{licence} ; "
        else:
            self.licence = ""
        #endIf
        self.licence += "Code source: GNU AFFERO GENERAL PUBLIC LICENSE"
    #endDef

    def to_dict(self):
        return {
            "date_creation_database":self.date,
            "url_api":self.url,
            "licence":self.licence
        }
    #endDef
            
#endClass
