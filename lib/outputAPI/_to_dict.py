def to_dict(self, removeEmpty=True):
    data = {
        "information":self.info,
        "reference":self.reference,
        "content":self.content,
        "status":self.status,
        "errors":self.errors,
    }

    if removeEmpty:
        for k in data.keys():
            if not data[k]:
                del data[k]
            #endIf
        #endFor
    else:
        for k in data.keys():
            if not data[k]:
                data[k] = None
            #endIf
        #endFor
    #endIf

    return data
#endDef
