from typing import Union
from dktotoolkit import recurs_function
from lib.outputAPI.informations import OutputAPI_informations

class OutputAPI:
    """
@brief: Cette classe contient toutes les donnees qui iront en sortie de l'API
"""
    def __init__(self, content:Union[str, dict]=None, reference:str=None, status:int=-999, errors:str=None, credit:str=None, informations:Union[str,dict]=None):
        """Constructor

        :param str,dict content: Input content
        :param str reference: The reference
        :param int status: The HTML response code
        :param str errors: The errors
        :param str credit: The credit
        """
        if informations is None :
            self.info = OutputAPI_informations(licence=credit).to_dict()
        else:
            self.info = informations
        #endIf
        self.reference=reference
        self.content=content
        self.errors=errors
        self.status=status

    #endDef

    from ._to_dict import to_dict

    def raw_string(self, data):
        if data is None or isinstance(data, (int, float, bool)):
            return data
        elif not isinstance(data, str):
            return recurs_function(
                self.raw_string,
                data=data
            )
        #
        return repr(data)
#endClass
