import os, sys
import hug

from dktotoolkit import load_dotenv

from mods.mod_bible import ModuleBible
from mods.mod_compendium import ModuleCompendium
from mods.mod_office import ModuleOffice

# https://hugapi.github.io/hug/reference/
# https://morioh.com/p/991c2160a8ee

load_dotenv("./.env")


# Liste des commandes :
# o : a faire
# x : fait
# - : en cours
#
#
# /v1
# | o index
# | - Bible                               : ModuleBible
# | | o index
# | | x Jn, Mc, ...
# | x Daily-Bible                         : ModuleBible  ==============> A passer dans /v1/Sequence-Sommaire/Bible1an
# | - Compendium                          : ModuleCompendium
#      options : ?dis=[desambiguation : libre]&lang=[francais/latin]
# | | - index
# | | - Prieres (oratio) : via bdd
# | | | o index
# | | | o pater,...
# | | o Hymnes (hymnus)  : via bdd
# | | | o index
# | | | o Avant la fin de la lumiere, ...
# | | o Antiennes (antiphona) : via bdd
# | | | o index
# | | | o Ant. cantique symeon, ...
# | | o Cantiques (canticum) : via bdd
# | | | o index
# | | | o Cantique symeon, ...
# | - Sequence (/ Sommaire pour uniquement le sommaire)  : ModuleCompendium
# | | o index
# | | o office
# | | | o index
# | | | o laudes, ...
# | | o messe
# | | o priere
# | | | o index
# | | | o defunts, ...


@hug.get("/v2/{name}")
@hug.local()
def say_hi(name: str):
    """Return a friendly message when visiting /v2/{name}"""
    return f"Hi {name}!"
#endDef



def host_port_url():
  # set host and port
  host = os.environ.get("API_HOST", "0.0.0.0")
  port = os.environ.get('API_PORT', 8000)
  url = os.environ.get('API_URL', 'localhost')


  if not url.startswith("/"):
    url = "/" + url
  #endIf
  if url.endswith("/"):
    url = url[:-1]
  #endIf

  url_clean = url.split(":")[0] if not "://" in url else url.split("://")[1].split(":")[0]

  return host, port, url_clean
#endDef


# catch error
@hug.sink('/all') #    @hug.extend_api()
def my_sink(request): # https://www.hug.rest/website/learn/routing
    return request.path.replace('/all', '')
#endDef

host, port, url = host_port_url()

route = hug.route.API(__name__)
route.object('/posts')(ModuleBible)  # a quoi sert le /posts ?
# route.object('/posts')(ModuleErrors)
route.object('/posts')(ModuleCompendium)
route.object('/posts')(ModuleOffice)

if __name__=="__main__":
    # Hug passera par ici.

    # pour fonctionner avec gunicorn
    # gunicorn --bind 0.0.0.0:8000 --daemon main:__hug_wsgi__
    # create your API
    my_api = hug.API(__name__)

    # set output format
    my_api.http.output_format = hug.output_format.json

    my_api.http.serve(host=host, port=int(port))
#

